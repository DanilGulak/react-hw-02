import * as Yup from "yup";

export const validationFormSchema = Yup.object().shape({

    firstName: Yup.string()
        .min(2, 'Too Short!')
        .max(50, 'Too Long!')
        .required('Required'),
    lastName: Yup.string()
        .min(2, 'Too Short!')
        .max(50, 'Too Long!')
        .required('Required'),
    age: Yup.number()
        .typeError('You must  specify a number!')
        .required('Required'),
    address: Yup.string()
        .required('Required'),
    tel: Yup.number()
        .typeError('You must  specify a number!')
        .required('Required')
})