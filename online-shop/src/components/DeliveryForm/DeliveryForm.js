import React from 'react';
import {Form, Formik, Field} from 'formik';
import './DeliveryForm.scss'
import {useDispatch, useSelector} from "react-redux";
import {setCards, setUserInfo} from "../../store/actions";
import {getCards} from "../../store/selectors";
import {validationFormSchema} from "./deliveryFormValidation";
import PropTypes from "prop-types";



const DeliveryForm = ({inCartItems}) => {

    const dispatch = useDispatch()
    const cards = useSelector(getCards)
    console.log(cards)


    const submitForm = (values) => {

        const {firstName, lastName, age, address, tel} = values

        console.log(`Name: ${firstName}, Last name: ${lastName}, Age: ${age}, Address: ${address}, Mobile number: ${tel}`)
        console.group(`Your cart is - `)


        inCartItems.forEach((e) => {
            console.log(`Author: ${e.author} Name: ${e.name}`)
        })

        console.groupEnd()

        const newCardsArray = cards.map((e) => {
            e.inCart = false

            return e
        })


        localStorage.removeItem('cartItems')

        dispatch(setCards(newCardsArray))
        dispatch(setUserInfo(values))



        const inputsList = document.querySelectorAll('input')
        inputsList.forEach(e => e.value = '')
    }

    return (
        <div className='delivery-form'>
            <Formik
                initialValues={{
                    firstName: '',
                    lastName: '',
                    age: '',
                    address: '',
                    tel: ''
                }}
                onSubmit={submitForm}
                validationSchema={validationFormSchema}
            >
                {(formikProps) => {

                    const {errors, touched} = formikProps

                    return (
                        <Form className='delivery-form__form'>

                            <label className='delivery-form__label'>
                                Name
                                <Field
                                    type='text'
                                    name='firstName'
                                    value={formikProps.values.firstName}
                                />
                            </label>
                            {errors.firstName && touched.firstName &&
                            <span className='form-error'>{errors.firstName}</span>}

                            <label className='delivery-form__label'>
                                Last name
                                <Field
                                    type='text'
                                    name='lastName'
                                    value={formikProps.values.lastName}
                                />
                            </label>
                            {errors.lastName && touched.lastName &&
                            <span className='form-error'>{errors.lastName}</span>}

                            <label className='delivery-form__label'>
                                Age
                                <Field
                                    type='text'
                                    name='age'
                                    value={formikProps.values.age}
                                />
                            </label>
                            {errors.age && touched.age &&
                            <span className='form-error'>{errors.age}</span>}

                            <label className='delivery-form__label'>
                                Delivery address
                                <Field
                                    type='text'
                                    name='address'
                                    value={formikProps.values.address}
                                />
                            </label>
                            {errors.address && touched.address &&
                            <span className='form-error'>{errors.address}</span>}

                            <label className='delivery-form__label'>
                                Mobile number
                                <Field
                                    type='text'
                                    name='tel'
                                    value={formikProps.values.tel}
                                />
                            </label>
                            {errors.tel && touched.tel &&
                            <span className='form-error'>{errors.tel}</span>}

                            <div>
                                <button type='submit' className='delivery-form__submit-btn'>Checkout</button>
                            </div>
                        </Form>
                    )
                }}
            </Formik>
        </div>
    );

};

DeliveryForm.propTypes = {
    cards: PropTypes.array,
    inCartItems: PropTypes.array
}

DeliveryForm.defaultProps = {
    cards: [],
    inCartItems: []
}


export default DeliveryForm;
