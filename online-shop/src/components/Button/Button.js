import React  from 'react';
import './Button.scss'
import PropTypes from "prop-types";

const Button = (props) => {

    const {text, bgColor, className, onClick , disabled} = props



    return (
            <button
                style={{backgroundColor: bgColor}}
                onClick={onClick}
                className={className}
                disabled={disabled}
            >
                {text}
            </button>
    );
};

Button.propTypes = {
    text: PropTypes.string,
    bgColor: PropTypes.string,
    className: PropTypes.string,
    onClick: PropTypes.func,
    disabled: PropTypes.bool
}

Button.defaultProps = {
    bgColor: 'transparent',
    text: 'Default button text',
    className: 'btn',
    disabled: false
}

export default Button;