import React from 'react';
import './Footer.scss'

const Footer = () => {
    return (
        <footer className='footer'>
            <span>&copy; 2021 Musicboard AB. All rights reserved.</span>
            <span>1,493,666 ratings recorded</span>
        </footer>
    );
};

export default Footer;