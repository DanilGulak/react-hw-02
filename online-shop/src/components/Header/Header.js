import React from 'react';
import './Header.scss'
import { NavLink } from "react-router-dom";

const Header = () => {
    return (
        <header className='header'>
            <div className="logo logo-wrapper">
                <img src="https://pbs.twimg.com/profile_images/1270859383254200323/2yUvfaOy.jpg" alt="logo" className="logo__img"/>
                <span className="logo__text">musicboard</span>
            </div>

            <div className="navbar">
                <ul className="navbar__list">
                    <li><NavLink className="navbar__items" activeClassName='navbar__items_is-active' to='/home'>Home</NavLink></li>
                    <li><NavLink className="navbar__items" activeClassName='navbar__items_is-active' to='/favorites'>Favorites</NavLink></li>
                    <li><NavLink className="navbar__items" activeClassName='navbar__items_is-active' to='/cart'>Cart</NavLink></li>
                </ul>
            </div>
        </header>
    );
};

export default Header;


