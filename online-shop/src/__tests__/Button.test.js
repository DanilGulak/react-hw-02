import Button from "../components/Button/Button";
import {render} from '@testing-library/react'
import userEvent from "@testing-library/user-event";


describe('Testing Button.js', () => {

    test('Smoke test Button.js', () => {
        render(<Button/>)
    })

    test('should be called with "ADD TO CARD" text', () => {

        const clickBtn = jest.fn()

        const {getByText} = render(<Button onClick={clickBtn} text='Add to cart'/>)

        expect(clickBtn).not.toHaveBeenCalled()

        userEvent.click(getByText('Add to cart'))

        expect(getByText('Add to cart').textContent).toBe('Add to cart')
        expect(clickBtn).toHaveBeenCalled()
        expect(clickBtn).toHaveBeenCalledTimes(1)

    })

    test('should be called with "YES" text', () => {

        const clickBtn = jest.fn()

        const {getByText} = render(<Button onClick={clickBtn} text='YES'/>)

        expect(clickBtn).not.toHaveBeenCalled()

        userEvent.click(getByText('YES'))

        expect(getByText('YES').textContent).toBe('YES')
        expect(clickBtn).toHaveBeenCalled()
        expect(clickBtn).toHaveBeenCalledTimes(1)

    })

    test('should be called with "NO" text', () => {

        const clickBtn = jest.fn()

        const {getByText} = render(<Button onClick={clickBtn} text='NO'/>)

        userEvent.click(getByText('NO'))

        expect(getByText('NO').textContent).toBe('NO')

    })

})