export const getCards = state => state.cards
export const getCardItem = state => state.cardItem
export const getIsOpen = state => state.modal.isOpen
export const getIsOpenCart = state => state.modal.isOpenCart
export const getUserInfo = state => state.userInfo