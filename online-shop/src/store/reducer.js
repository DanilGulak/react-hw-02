import {
    SET_CARD,
    SET_CARDS,
    SET_IS_OPEN_MODAL,
    SET_IS_OPEN_MODAL_CART,
    SET_USER_INFO
} from "./types";

const initialStore = {
    cards: [],
    cardItem: null,
    modal: {
        isOpen: false,
        isOpenCart: false
    },
    userInfo: null
}


const reducer = (state = initialStore, action) => {

    switch (action.type) {
        case SET_CARDS: {
            return {...state, cards: action.payload}
        }
        case SET_CARD: {
            return {...state, cardItem: action.payload}
        }
        case SET_IS_OPEN_MODAL: {
            return {...state, modal: {...state.modal, isOpen: action.payload}}
        }
        case SET_IS_OPEN_MODAL_CART: {
            return {...state, modal: {...state.modal, isOpenCart: action.payload}}
        }
        case SET_USER_INFO: {
            return {...state, userInfo: action.payload}
        }
        default: {
            return state
        }
    }
}

export default reducer
