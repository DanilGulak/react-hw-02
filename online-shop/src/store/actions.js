import {
    SET_CARD,
    SET_CARDS,
    SET_IS_OPEN_MODAL,
    SET_IS_OPEN_MODAL_CART,
    SET_USER_INFO
} from "./types";

export const setCards = (data) => {
    return {type: SET_CARDS, payload: data}
}
export const setCard = (data) => {
    return {type: SET_CARD, payload: data}
}
export const setIsOpenModal = (data) => {
    return {type: SET_IS_OPEN_MODAL, payload: data}
}
export const setIsOpenModalCart = (data) => {
    return {type: SET_IS_OPEN_MODAL_CART, payload: data}
}

export const setUserInfo = (data) => {
    return {type: SET_USER_INFO, payload: data}
}