import axios from "axios";
import {setCards} from "./actions";

const normalizeData = (data) => {

    return data.map(el => {
        const favorites = JSON.parse(localStorage.getItem('setFavorite')) || []
        const cart = JSON.parse(localStorage.getItem('cartItems')) || []
        el.inFavorite = favorites.includes(el.setNumber)
        el.inCart = cart.includes(el.setNumber)
        return el
    })
}

export const getCardsData = () => (dispatch) => {
    axios('/songs.json')
        .then(res => {
            const normalizeArray = normalizeData(res.data)
            dispatch(setCards(normalizeArray))
        })
}