import React from 'react';
import Card from "../../components/Card/Card";
import './CardList.scss'
import PropTypes from "prop-types";
import {useSelector} from "react-redux";
import {getCards} from "../../store/selectors";


const CardList = (props) => {

    const cards = useSelector(getCards)

    const {toggleFavorite} = props


    return (
        <div>
            <section className='cards-container'>

                {cards.map(card => {
                    return (
                        <Card
                            key={card.setNumber}
                            card={card}
                            toggleFavorite={toggleFavorite}
                        />
                    )
                })}

            </section>
        </div>
    );
};

CardList.propTypes = {
    cards: PropTypes.array,
    updateCards: PropTypes.func
}

CardList.defaultProps = {
    cards: []
}

export default CardList;