import React from 'react';
import Card from "../../components/Card/Card";
import PropTypes from "prop-types";
import {useSelector} from "react-redux";
import {getCards} from "../../store/selectors";
import DeliveryForm from "../../components/DeliveryForm/DeliveryForm";

const Cart = (props) => {

    const cards = useSelector(getCards)
    const {toggleFavorite} = props

    const inCartItems = cards.filter(e => e.inCart)

    return (
        <React.Fragment>

            <section className='cards-container'>

                {inCartItems.map(item => {

                    return (
                        <Card
                            key={item.setNumber}
                            card={item}
                            toggleFavorite={toggleFavorite}
                        />
                    )

                })}

            </section>

            {inCartItems.length > 0 && <DeliveryForm inCartItems={inCartItems}/>}

        </React.Fragment>
    );
};

Cart.propTypes = {
    cards: PropTypes.array,
    updateCards: PropTypes.func
}

Cart.defaultProps = {
    cards: []
}

export default Cart;
