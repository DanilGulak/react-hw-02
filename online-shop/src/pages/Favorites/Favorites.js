import React from 'react';
import Card from "../../components/Card/Card";
import PropTypes from "prop-types";
import {useSelector} from "react-redux";
import {getCards} from "../../store/selectors";

const Favorites = (props) => {

    const cards = useSelector(getCards)

    const {toggleFavorite} = props

    const inFavoriteItems = cards.filter(e => e.inFavorite)

    return (
        <React.Fragment>

            <section className='cards-container'>

                {inFavoriteItems.map(item => {

                    return (
                        <Card
                            key={item.setNumber}
                            card={item}
                            toggleFavorite={toggleFavorite}
                        />
                    )

                })}

            </section>

        </React.Fragment>
    );
};

Favorites.propTypes = {
    cards: PropTypes.array,
    updateCards: PropTypes.func
}

Favorites.defaultProps = {
    cards: []
}

export default Favorites;
