import React from 'react';
import {Redirect, Route, Switch} from "react-router-dom";
import Favorites from "../pages/Favorites/Favorites";
import Cart from "../pages/Cart/Cart";
import CardList from "../pages/Home/CardList";
import Error404 from "../components/Error404/Error404";
import PropTypes from "prop-types";

const AppRoutes = (props) => {

    const {toggleFavorite} = props

    return (
        <div>
            <Switch>
                <Redirect exact from='/' to='/home'/>
                <Route exact path='/home'  render={() => <CardList toggleFavorite={toggleFavorite} />}/>
                <Route exact path='/favorites' render={() => <Favorites toggleFavorite={toggleFavorite} />}/>
                <Route exact path='/cart' render={() => <Cart toggleFavorite={toggleFavorite} />}/>
                <Route path='*' component={Error404}/>
            </Switch>
        </div>
    );
};

AppRoutes.propTypes = {
    cards: PropTypes.array,
    updateCards: PropTypes.func
}

AppRoutes.defaultProps = {
    cards: []
}

export default AppRoutes;
