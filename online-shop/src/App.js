import React, {useEffect} from 'react';
import './App.scss';
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import AppRoutes from "./routes/AppRoutes";
import {useDispatch, useSelector} from "react-redux";
import {setCards, setIsOpenModal, setIsOpenModalCart} from "./store/actions";
import Modal from "./components/Modal/Modal";
import {getCardItem, getCards, getIsOpen, getIsOpenCart} from "./store/selectors";
import {getCardsData} from "./store/operations";

const App = () => {

    const cards = useSelector(getCards)
    const cardItem = useSelector(getCardItem)
    const isOpen = useSelector(getIsOpen)
    const isOpenCart = useSelector(getIsOpenCart)
    const dispatch = useDispatch()


    useEffect(() => {
        dispatch(getCardsData())
    }, [dispatch])


    const setItems = (number, key) => {
        let itemList = JSON.parse(localStorage.getItem(key)) || []
        itemList.push(number)
        localStorage.setItem(key, JSON.stringify(itemList))
    }

    const removeItems = (number, key) => {
        let itemList = JSON.parse(localStorage.getItem(key)) || []
        const removeItem = itemList.find(el => el === number)
        const indexRemove = itemList.indexOf(removeItem)
        itemList.splice(indexRemove, 1)
        localStorage.setItem(key, JSON.stringify(itemList))
    }

    const toggleCart = (number, key) => {
        const newArray = cards.map(element => {

            if (element.setNumber === number) {
                element.inCart = !element.inCart

                if (element.inCart) {
                    setItems(number, key)
                    // setIsOpen(!isOpen)
                    dispatch(setIsOpenModal(!isOpen))
                } else {
                    removeItems(number, key)
                    // setIsOpenCart(!isOpenCart)
                    dispatch(setIsOpenModalCart(!isOpenCart))
                }
            }
            return element
        })
        dispatch(setCards(newArray))
    }


    const toggleFavorite = (number, key) => {

        const newArray = cards.map(element => {

            if (element.setNumber === number) {
                element.inFavorite = !element.inFavorite

                if (element.inFavorite) {
                    setItems(number, key)
                } else {
                    removeItems(number, key)
                }
            }
            return element
        })
        dispatch(setCards(newArray))
    }


    return (
        <div>
            <Header/>
            <AppRoutes toggleFavorite={toggleFavorite}/>
            <Footer/>
            {isOpen && <Modal
                header="Do you want to add to cart?"
                text='This song will be added to your cart!'
                onClick={() => dispatch(setIsOpenModal(!isOpen))}
                cross
                actions={
                    {
                        okButton: () => (<button onClick={() => toggleCart(cardItem.setNumber, 'cartItems')}
                                                 className="btn btn-ok">Yes</button>),
                        exitButton: () => (
                            <button onClick={() => dispatch(setIsOpenModal(!isOpen))}
                                    className="btn btn-cancel">No</button>)
                    }
                }
            />}
            {isOpenCart && <Modal
                header="Do you want to remove from cart?"
                text='This song will be removed from your cart!'
                onClick={() => dispatch(setIsOpenModalCart(!isOpenCart))}
                cross
                actions={
                    {
                        okButton: () => (<button onClick={() => toggleCart(cardItem.setNumber, 'cartItems')}
                                                 className="btn btn-ok">Yes</button>),
                        exitButton: () => (
                            <button onClick={() => dispatch(setIsOpenModalCart(!isOpenCart))}
                                    className="btn btn-cancel">No</button>)
                    }
                }
            />}
        </div>
    );
};
export default App;